const express = require('express');
const fs = require('fs');
const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');
const jwt = require('jsonwebtoken');
const task = require('./js/task.js');
const category = require('./js/category.js');
const auth = require('./js/auth.js');
const comment = require('./js/comment.js');
const status = require('./js/status.js');


const app = express();
const PORT = process.env.PORT || 5001;
const JWT = process.env.JWT || 'abcddefghijk';
app.listen(PORT, () => { `Server is running on ${PORT}` });
app.get('/', (req,res) => {res.redirect('main')});


app.use(express.static('../client'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


// Middleware zum Tokenüberprüfen

const checkToken = (req, res, next) => {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const token = bearerHeader.split(' ')[1];
        jwt.verify(token, JWT, (err, decoded) => {
            if (err) {
                return res.status(403).send('Invalid token');
            }
            req.username = decoded;
            res.status(200).send('Valid Token')
            next();
        });
    } else {
        res.status(401).send('No token found');
    }
};


// Auth
app.post('/register', auth.registerUser);
app.post('/login', auth.loginUser);
app.post('/logout', auth.logoutUser);
app.get('/username', auth.getUsername);
app.patch('/updateUser', auth.updateUser);
app.get('/checkToken', checkToken)

// Task and Timer
app.get('/allTasks', task.allTasks);
app.get('/countFinishedTask', task.countFinishedTask);
app.get('/getTime/:id',  task.getTime);
app.get('/countOpenTask',  task.countOpenTask);
app.get('/countPendingTasks',  task.countPendingTasks);
app.patch('/saveTimer/:id',  task.saveTimer);
app.post('/addTask', task.addTask);
app.delete('/deleteTask/:id',  task.deleteTask);
app.patch('/updateTask/:id',  task.updateTask);

// Category
app.get('/allCategory', category.allCategory);
app.post('/addCategory', category.addCategory);
app.delete('/deleteCategory/:id', category.deleteCategory);

// STATUS 
app.get('/allStatus', status.allStatus);

// COMMENT 
app.get('/getComment/:id', comment.getComment);
app.post('/commentTask/:id', comment.commentTask);
app.patch('/updateComment/:id', comment.updateComment)



