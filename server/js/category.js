const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const CATEGORY = './js/data/category.json';


const addCategory = async (req, res) => {

    let newCat = JSON.parse(await fs.promises.readFile(CATEGORY));
    let foundCat = newCat.find(newCat => newCat.cat === req.body.cat);
    if (foundCat) {
        res.status(400).send('Kategorie bereits vorhanden');
    }
    if (!foundCat) {
        let addCat = {
            id: uuidv4(),
            cat: req.body.cat
        }
        newCat.push(addCat);
        await fs.promises.writeFile(CATEGORY, JSON.stringify(newCat, null, "\t"));
        res.status(200).send('Kategorie hinzugefügt');
    } else {
        res.status(404).send('Fehler beim hinzufügen');
    }
}

const allCategory = async (req, res) => {

    let category = JSON.parse(await fs.promises.readFile(CATEGORY));
    if (category.length > 0) {
        res.status(200).send(category);
    } else {
        res.status(204).send('Keine Kategorien gefunden');
    }
}

const deleteCategory = async (req, res) => {
    
    let delCat = JSON.parse(await fs.promises.readFile(CATEGORY))
    let foundCat = delCat.find(delCat => delCat.id === req.params.id);

    if (foundCat !== -1) {
        delCat.splice(foundCat, 1);
        await fs.promises.writeFile(CATEGORY, JSON.stringify(delCat, null, "\t"));
        res.status(200).send('Kategorie wurder gelöscht');
    } else {
        res.status(404).send('Kategorie nicht gefunden')
    }
}



module.exports = {
    addCategory,
    allCategory,
    deleteCategory
}