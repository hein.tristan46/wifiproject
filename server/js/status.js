const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const STATUSFILE = './js/data/status.json';



const allStatus = async (req, res) => {

    let status = JSON.parse(await fs.promises.readFile(STATUSFILE));
    if (status.length > 0) {
        res.status(200).send(status);
    } else {
        res.status(204).send('Keine Status gefunden');
    }
}



module.exports = {
    allStatus,
}