const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
var jwt = require('jsonwebtoken');
const { json } = require('express');
const { reset } = require('nodemon');


const TASKFILE = './js/data/task.json';
const FINISHEDTASK = './js/data/finishedTask.json';
const STATUSFILE = './js/data/status.json';
const PENDINGFILE = './js/data/pending.json';
const OPENFILE = './js/data/open.json';

const allTasks = async (req, res) => {
    let tasks = JSON.parse(await fs.promises.readFile(TASKFILE));
    if (tasks.length > 0) {
        return res.status(200).json(tasks);
    } else {
        return res.status(204).send('Keine Tasks gefunden');
    }
}

const addTask = async (req, res) => {

    let newTask = JSON.parse(await fs.promises.readFile(TASKFILE));
    let pending = JSON.parse(await fs.promises.readFile(PENDINGFILE));
    let open = JSON.parse(await fs.promises.readFile(OPENFILE));

    let foundTask = newTask.findIndex(newTask => newTask.task === req.body.task);
    if (foundTask !== -1) {
        return res.status(400).json('Task already in use');
    }

    let addTask = {
        id: uuidv4(),
        task: req.body.task,
        kategorie: req.body.category,
        status: req.body.status,
        time: 0
    }
    newTask.push(addTask);
    await fs.promises.writeFile(TASKFILE, JSON.stringify(newTask, null, "\t"));

    if (req.body.status === 'Bearbeitung') {
        let addPending = {
            id: addTask.id,
            task: req.body.task
        }
        pending.push(addPending);
        await fs.promises.writeFile(PENDINGFILE, JSON.stringify(pending, null, "\t"));
    }

    if (req.body.status === 'Offen') {
        let addOpen = {
            id: addTask.id,
            task: req.body.task
        }
        open.push(addOpen);
        await fs.promises.writeFile(OPENFILE, JSON.stringify(open, null, "\t"));
    }
    res.status(200).json('Task is added');

}

const countOpenTask = async (req, res) => {

    let data = await fs.promises.readFile(OPENFILE);
    let eintraege = JSON.parse(data);
    let anzahlEintraege = Object.keys(eintraege).length;
    if (!anzahlEintraege) {
        res.status(204).json({ message: 'Kein Eintrag gefunden' });
    } else {
        res.status(200).json({ anzahlEintraege });
    }
}

const deleteTask = async (req, res) => {

    try { 

    let delTask = JSON.parse(await fs.promises.readFile(TASKFILE));
    let saveTask = JSON.parse(await fs.promises.readFile(FINISHEDTASK));
    let resetPending = JSON.parse(await fs.promises.readFile(PENDINGFILE));
    let resetOpen = JSON.parse(await fs.promises.readFile(OPENFILE));

    let foundTask = delTask.findIndex(delTask => delTask.id === req.params.id);
    if (foundTask !== -1) {
        delTask.splice(foundTask, 1);
        saveTask.push({ id: req.params.id });
        await fs.promises.writeFile(TASKFILE, JSON.stringify(delTask, null, "\t"));
        await fs.promises.writeFile(FINISHEDTASK, JSON.stringify(saveTask, null, "\t"));
    }

    let foundPending = resetPending.findIndex(resetPending => resetPending.id === req.params.id);
    if (foundPending !== -1) {
        resetPending.splice(foundPending, 1);
        await fs.promises.writeFile(PENDINGFILE, JSON.stringify(resetPending, null, "\t"));
    }

    let foundOpen = resetOpen.findIndex(resetOpen => resetOpen.id === req.params.id);
    if (foundOpen !== -1) {
        resetOpen.splice(foundOpen, 1);
        await fs.promises.writeFile(OPENFILE, JSON.stringify(resetOpen, null, "\t"));
    }
    res.status(200).json({ message: '' });

    }catch (err) {
        res.status(500).send(err, 'Server fehler');
    }
}

const countFinishedTask = async (req, res) => {
    let data = await fs.promises.readFile(FINISHEDTASK);
    let eintraege = JSON.parse(data);
    let anzahlEintraege = Object.keys(eintraege).length;

    if (!anzahlEintraege) {
        res.status(204).send('Kein Eintrag gefunden');
    } else {
        res.json({ anzahlEintraege });
    }
}

const updateTask = async (req, res) => {
    let task = JSON.parse(await fs.promises.readFile(TASKFILE));
    let open = JSON.parse(await fs.promises.readFile(OPENFILE));
    let pending = JSON.parse(await fs.promises.readFile(PENDINGFILE));

    let foundTaskIndex = task.findIndex(task => task.id === req.params.id);

    if (foundTaskIndex !== -1) {
        task[foundTaskIndex].task = req.body.task;
        task[foundTaskIndex].kategorie = req.body.cat;
        task[foundTaskIndex].status = req.body.status;
        await fs.promises.writeFile(TASKFILE, JSON.stringify(task, null, "\t"));

        if (req.body.status == 'Offen') {
            let pendingTaskIndex = pending.findIndex(pendingTask => pendingTask.id === req.params.id);
            if (pendingTaskIndex !== -1) {
                pending.splice(pendingTaskIndex, 1);
            }
            await fs.promises.writeFile(PENDINGFILE, JSON.stringify(pending, null, "\t"));
            open.push(task[foundTaskIndex]);
            await fs.promises.writeFile(OPENFILE, JSON.stringify(open, null, "\t"));
        }
        
        if (req.body.status === 'Bearbeitung') {
            let openTaskIndex = open.findIndex(openTask => openTask.id === req.params.id);
            if (openTaskIndex !== -1) {
                open.splice(openTaskIndex, 1);
            }
            await fs.promises.writeFile(OPENFILE, JSON.stringify(open, null, "\t"));
            pending.push(task[foundTaskIndex]);
            await fs.promises.writeFile(PENDINGFILE, JSON.stringify(pending, null, "\t"));
        }
        res.status(200).send('Task updated');
    } else {
        res.status(404).send('Task not found');
    }
}

const getTime = async (req, res) => {
    let task = JSON.parse(await fs.promises.readFile(TASKFILE));
    let foundTimer = task.findIndex(task => task.id === req.params.id);

    if (foundTimer !== -1) {
        let time = {
            time: task[foundTimer].time
        }
        res.status(200).json({ time: time });
    } else {
        res.status(404).json({ message: 'Kein Eintrag gefunden' })
    }

}

const saveTimer = async (req, res) => {

    let newData = JSON.parse(req.body.data);
    let allTimes = JSON.parse(await fs.promises.readFile(TASKFILE));
    let foundTask = allTimes.findIndex(allTimes => allTimes.id === req.params.id);

    if (foundTask !== -1) {
        allTimes[foundTask].time = newData;

        await fs.promises.writeFile(TASKFILE, JSON.stringify(allTimes, null, "\t"));
        res.status(200).send('Saved')
    } else {
        res.status(404).send('Not found')
    }
}

const countPendingTasks = async (req, res) => {
    let data = await fs.promises.readFile(PENDINGFILE);
    let eintraege = JSON.parse(data);
    let anzahlEintraege = Object.keys(eintraege).length;

    if (!anzahlEintraege) {
        res.status(204).send('Keine Eintrag gefunden');
    } else {
        res.status(200).json({ anzahlEintraege });
    }
}


module.exports = {
    allTasks,
    addTask,
    countOpenTask,
    deleteTask,
    updateTask,
    countFinishedTask,
    countPendingTasks,
    getTime,
    saveTimer
};