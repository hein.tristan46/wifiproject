const crypto = require('crypto');
const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');


const USERFILE = './js/data/users.json';
const app = express();
const JWT = process.env.JWT || 'abcddefghijk';


const registerUser = async (req, res) => {
    let users = JSON.parse(await fs.promises.readFile(USERFILE));
    let foundUser = users.find(users => users.username === req.body.username)
    if (foundUser) {
        res.status(400).json({ message: 'User already exists' });
        return
    }
    if (!foundUser) {

        const hashPassword = await bcrypt.hash(req.body.password, 10);

        let newUser = {
            id: uuidv4(),
            username: req.body.username,
            password: hashPassword
        }
        users.push(newUser);
        await fs.promises.writeFile(USERFILE, JSON.stringify(users, null, "\t"));
        res.status(200).send('')
    } else {
        res.status(404).send('User konnte nicht angelegt werden')
    }
}

const loginUser = async (req, res) => {
    let users = JSON.parse(await fs.promises.readFile(USERFILE));
    let foundUser = await users.find(users => users.username == req.body.username);
    if (foundUser && await bcrypt.compare(req.body.password, foundUser.password)) {
        const token = jwt.sign({ username: req.body.username }, JWT, { expiresIn: "2h" });
        res.status(200).send({ token: token });
    } else {
        res.status(401).json('Falscher User/Passwort')
    }
}

const updateUser = async(req,res) => {

    let users = JSON.parse(await fs.promises.readFile(USERFILE));
    let searchUser = users.find(users => users.id === req.params.id) 

    if(searchUser) {
        let updateUser = {
            id: req.params.id,
            vorname: req.body.vorname,
            nachname: req.body.nachname,
            smText: req.body.smText,
            Text: req.body.text,
            location: req.body.location,
            phone: req.body.phone,
            position: req.body.position
        }
        users.push(updateUser);
        await fs.promises.writeFile(USERFILE, JSON.stringify(users, null, "\t"));
        res.status(200).send('User updated');
    }else {
        res.status(404).send('User not found')
    }
}

const logoutUser = (req, res) => {
    
    try {
        res.clearCookie('jwt')
        res.status(200).send('Logout erfolgreich');
    } catch (err) {
        res.status(500).send('Server errror')
    }
}

const getUsername = async (req, res) => {
    let users = await (fs.promise.readFile(USERFILE))
    let username = await users.find(users => users.username == req.body.username);

    if (username) {
        res.status(200).send({ username });
    } else {
        res.status(404).send('USer not found')
    }
}

module.exports = {
    registerUser,
    loginUser,
    logoutUser,
    getUsername,
    updateUser
};