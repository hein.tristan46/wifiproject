const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
var jwt = require('jsonwebtoken');


const COMMENT = './js/data/comment.json';
const TASKFILE = './js/data/task.json';


const commentTask = async (req, res) => {

    let comment = JSON.parse(await fs.promises.readFile(COMMENT));
    let task = JSON.parse(await fs.promises.readFile(TASKFILE));
    let searchTask = task.findIndex(task => task.id === req.params.id);

    if (searchTask !== -1) {
        let newComment = {
            commentId: req.params.id,
            comment: req.body.comment,
            username: req.body.username
        };

        comment.push(newComment);
        task[searchTask].commentId = newComment.commentId;

        await fs.promises.writeFile(COMMENT, JSON.stringify(comment, null, "\t"));
        await fs.promises.writeFile(TASKFILE, JSON.stringify(task, null, "\t"));

        res.status(200).json({message: newComment});
    } else {
        res.status(204).json({message: 'kein Kommentar gefunden'});
    }
}

const getComment = async (req, res) => {

    let comment = JSON.parse(await fs.promises.readFile(COMMENT));
    let foundComments = comment.filter(comment => comment.commentId === req.params.id);

    if (foundComments.length > 0) {
        res.status(200).json({foundComments});
    }
    if(!foundComments) {
        res.status(404).json({Message: 'No comment found'});
    }
}

const updateComment = async(req,res) => {

    let comments = JSON.parse(await fs.promises.readFile(COMMENT));
    let foundTask = comments.findIndex(comment => comment.commentId === req.params.id);

    if(foundTask !== -1) {
        comments[foundTask].comment = req.body.comment;

        await fs.promises.writeFile(COMMENT, JSON.stringify(comments, null, "\t"))
        res.status(200).send('Comment updated');
    }else {
        res.status(404).send('Keinen Eintrag gefunden')
    }
}


module.exports = {
    commentTask,
    getComment,
    updateComment
}