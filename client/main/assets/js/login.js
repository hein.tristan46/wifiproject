const loginHandler = (e) => {
    e.preventDefault();
    let status = document.querySelector('#status');
    let username = document.querySelector('#un').value.trim();
    let password = document.querySelector('#pw').value;

    if (username == '' || password == '') {
        status.style.display = 'block';
        status.innerHTML = 'Bitte geben Sie einen Wert ein';
        status.style.color = 'red';
    } else {

        fetch('/login', {
            method: 'post',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: `username=${username}&password=${password}`
        })
            .then(resp => resp.json())
            .then(data => {
                if (data.token) {
                    status.style.color = '';
                    status.style.display = 'block';
                    status.innerHTML = 'Login war erfolgreich';
                    status.classList.add('success');
                    sessionStorage.setItem('jwt', data.token);
                    sessionStorage.setItem('user', username)
                    setTimeout(() => {
                        localStorage.removeItem('authenticated');
                        top.location.href = '../dashboard/index.html';
                    }, 2000);
                
                } else {
                    status.style.color = '';
                    status.style.display = 'block';
                    status.innerHTML = 'Login war nicht erfolgreich';
                    status.style.color = 'red';
                }
            })
    }
}

const authenticatHandler = () => {
    let authenticated = localStorage.getItem('authenticated');
    let status = document.querySelector('#status')

    if (authenticated === 'false') {
        status.style.display = 'block';
        status.innerHTML = 'Sie sind nicht angemeldet';
        status.style.color = 'red';
    }
}


let clickHandler = () => {
    document.querySelector('#btn').addEventListener('click', loginHandler)
    authenticatHandler();
}
clickHandler()