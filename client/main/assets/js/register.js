const registerHandler = (e) => {
    e.preventDefault();
    let username = document.querySelector('#username').value.trim();
    let password = document.querySelector('#password').value;
    let confirmPassword = document.querySelector('#re-password').value;
    let status = document.querySelector('#status');
    let message = document.querySelector('#status');
    let modal = document.querySelector('#myModal');

    if (password != confirmPassword) {
        message.innerHTML = 'Passwörter stimmen nicht überein';
        status.style.display = 'block';
        status.style.color = 'red';
    
    }else if(username === '' || password == '') {
        message.innerHTML = 'Bitte geben Sie einen Wert ein';
        status.style.display = 'block';
        status.style.color = 'red';
    
    }else {
        fetch('/register', {
            method: 'post',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: `username=${username}&password=${password}`
        }).then(data => {
            if (data) {
                modal.style.display = 'block';
                status.style.display = 'block';
                status.innerHTML = 'User registered'
                status.style.color = 'green';
            } else {
                message.innerHTML = 'Fehler beim registrieren';
                status.style.display = 'block';
                status.style.color = 'red';
            }
        })
    }
}

const clickHandler = () => {
    document.querySelector('#btn').addEventListener('click', registerHandler);
}

clickHandler();