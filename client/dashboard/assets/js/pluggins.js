
// COUNTER 

const taskCountHandler = async () => {
    let count = document.querySelector('#count');
    fetch('/countOpenTask', {
        method: 'get',
    }).then(resp => resp.json())
        .then(data => {
            if (!data) {

            } else {
                count.innerHTML = data.anzahlEintraege;
                countPendingTasks(count)
            }
        })
}

const countPendingTasks = async (tasks) => {

    let count = document.querySelector('#count-categorie')
    fetch('/countPendingTasks', {
        method: 'get'
    }).then(resp => resp.json())
        .then(data => {
            if (!data) {
                console.log('Kein Task vorhanden')
            } else {
                count.innerHTML = data.anzahlEintraege;
                finishedCounterHandler(tasks, count)
            }
        })
}

const finishedCounterHandler = async (tasks, pending) => {

    let count = document.querySelector('#count-deleted')
    fetch('/countFinishedTask', {
        method: 'get',
    }).then(resp => resp.json())
        .then(data => {
            if (!data) {
            
            } else {
                count.innerHTML = data.anzahlEintraege;
                if (window.location.pathname === '/dashboard/index.html') {
                    chartHandler(tasks, pending, count);
                }
            }
        })
}


// MAPS 

const createMapTask = (ul, data) => {
    const processedData = data.map(data => {
        return {
            id: data.id,
            task: data.task,
            kategorie: data.kategorie,
            time: data.time,
            commentId: data.commentId,
            status: data.status
        };
    });
    
    if (window.location.pathname === '/dashboard/index.html') {
        //limitedTaskList(ul, processedData);
        processedData.forEach(data => {
            bgListTask(data);
        });
    }

    else if (window.location.pathname === '/dashboard/task.html') {
        //limitedTaskList(ul, processedData);
        processedData.forEach(data => {
            bgListTask(data);
        });
    
        const lastFourData = processedData.slice(-4);
    
        lastFourData.forEach(data => {
            smListTask(ul, data);
        });
    }
}

const createMapCategory = (data) => {

    const processedData = data.map(data => {
        return {
            id: data.id,
            cat: data.cat,
        };
    });

    infoCardCategory(processedData, data)
}

const createMapStatus = (data) => {

    const processedData = data.map(data => {
        return {
            id: data.statusId,
            status: data.status
        };
    });

    infoCardStatus(processedData, data)
}


// LIST

const smListTask = (ul, data) => {
    if (ul.querySelectorAll('.task').length < 4) {
        let updateButton = document.querySelector('#btn-update-task');
        let finishButton = document.querySelector('#btn-finish-task');
        let li = document.createElement('li');
        let p = document.createElement('p');
        let line = document.createElement('hr');

        li.innerHTML = `${data.task}`;
        li.id = 'task';
        p.innerHTML = `Status: ${data.status}`;

        li.classList.add('task');
        p.classList.add('email-task');

        ul.appendChild(li);
        li.appendChild(p);
        li.appendChild(line);

        createClickHandler(data, li, p, updateButton, finishButton);
    }
}

const bgListTask = (data) => {

    let listTasks = document.querySelector('#list-task-second');
    let category = document.querySelector('#list-task-category');
    let status = document.querySelector('#list-task-status');
    let time = document.querySelector('#list-task-time');
    let updateButton = document.querySelector('#btn-update-task');
    let finishButton = document.querySelector('#btn-finish-task');
    let timeApp = document.querySelector('#time-app');

    let taskList = document.createElement('li');
    let categoryList = document.createElement('li');
    let listStatus = document.createElement('li');

    taskList.innerHTML = `${data.task}`;
    categoryList.innerHTML = `${data.kategorie}`;
    listStatus.innerHTML = `${data.status}`;

    listTasks.appendChild(taskList);
    category.appendChild(categoryList);

    status.appendChild(listStatus);

    if (window.location.pathname === '/dashboard/index.html') {
        taskList.addEventListener('click', () => {
            timeApp.innerHTML = data.task;

            let timeObject = {
                id: data.id,
                time: data.time
            }

            let timeString = JSON.stringify(timeObject)
            localStorage.setItem('timer', timeString);
            reqTime(data);
            
        })
    }
    if (window.location.pathname === '/dashboard/task.html') {
        taskList.addEventListener('click', () => {

            timeApp.innerHTML = data.task;

            let timeObject = {
                id: data.id,
                time: data.time
            }
            let timeString = JSON.stringify(timeObject)
            localStorage.setItem('timer', timeString);

            categoryHandler();
            cardInfo(data);
            reqTime(data);
            setTimerClick(data)

            updateButton.addEventListener('click', () => {
                updateTask(data);
                //commentHandler(data);
            });
            finishButton.addEventListener('click', () => {
                deleteTask(data);
            });
        });
    }
}

const limitedTaskList = (ul, processedData) => {
    let cat = document.querySelector('#select-category');
    let timeApp = document.querySelector('#time-app');
    ul.innerHTML = '';

    for (let i = 0; i < Math.min(processedData.length, 3); i++) {
        let li = document.createElement('li');
        let p = document.createElement('p');
        let line = document.createElement('hr');

        li.innerHTML = `${processedData[i].task}`;
        p.innerHTML = `Kategorie: ${processedData[i].kategorie}`;

        li.classList.add('task');
        p.classList.add('email-task');

        ul.appendChild(li);
        li.appendChild(p);
        li.appendChild(line);

    }
}


// CLICKHANDLER

const createClickHandler = async (data, li) => {

    let unselect = null;
    let updateButton = document.querySelector('#btn-update-task');
    let finishButton = document.querySelector('#btn-finish-task');
    let saveTimeButton = document.querySelector('#saveBtn');

    
    let allTasks = document.querySelectorAll('#task');
    let commentTask = document.querySelector('#show-all-comments');
    let addNewComment = document.querySelector('#addCommentBtn');
    let timeApp = document.querySelector('#time-app');

        li.addEventListener('click', () => {
            allTasks.forEach(task => {
                if (task !== li) {
                    task.classList.remove('selected');
                }
            });
            li.classList.add('selected');
            timeApp.innerHTML = data.task;
            
            let timeObject = {
                id: data.id,
                time: data.time
            }
            let timeString = JSON.stringify(timeObject)
            localStorage.setItem('timer', timeString);

            categoryHandler();
            cardInfo(data);
            reqTime(data);
            setTimerClick(data)

            /*
                    commentTask.addEventListener('click', () => {
                        commentModalHandler();
                        getComments(data);
            
                        addNewComment.addEventListener('click', () => {
                            let comment = document.getElementById('comment').value;
                            newComment(comment, data)
                        });
                    })*/

            updateButton.addEventListener('click', () => {
                updateTask(data);
                //commentHandler(data);
            });

            finishButton.addEventListener('click', () => {
                deleteTask(data);
            });
        });
}


// CARD_ADD_TASK - INFO_CARD

const cardInfo = async (data) => {

    let infoCard = document.querySelector('#card-task-info');
    let addTask = document.querySelector('#card-bg-task-info');
    let preTask = document.querySelector('#updateTask');
    let task = document.querySelector('#task-text');

    preTask.placeholder = data.task;
    task.innerHTML = data.task;

    if (infoCard.style.display === 'none' || infoCard.style.display === '') {
        infoCard.style.display = 'block';
        addTask.style.display = 'none';
    }
    document.querySelector('#close-btn').addEventListener('click', () => {
        infoCard.style.display = 'none';
        addTask.style.display = 'block';

    });
}

const infoCardCategory = (processedData) => {
    if (window.location.pathname === '/dashboard/task.html') {
        let select = document.querySelector('#select-category');
        select.innerHTML = '';
    }
    let infoSelect = document.querySelector('#info-select-category')
    infoSelect.innerHTML = '';

    processedData.forEach(data => {

        if (window.location.pathname === '/dashboard/index.html' || window.location.pathname === '/dashboard/task.html') {
            let infoOption = document.createElement('option');
            infoOption.innerHTML = `${data.cat}`;
            infoSelect.appendChild(infoOption);
        }

        if (window.location.pathname === '/dashboard/task.html') {
            let select = document.querySelector('#select-category');
            let option = document.createElement('option');
            option.innerHTML = `${data.cat}`;
            select.appendChild(option);
        }
    })

}

const infoCardStatus = (processedData) => {
    if (window.location.pathname === '/dashboard/task.html') {
        let selectStatus = document.querySelector('#select-status');
        selectStatus.innerHTML = '';
    }
    let infoStatus = document.querySelector('#info-select-status');

    processedData.forEach(data => {

        if (window.location.pathname === '/dashboard/index.html' || window.location.pathname === '/dashboard/task.html') {
            let infoStatusOption = document.createElement('option');
            infoStatusOption.innerHTML = `${data.status}`;
            infoStatus.appendChild(infoStatusOption);
        }

        if (window.location.pathname === '/dashboard/task.html') {
            let selectStatus = document.querySelector('#select-status');
            let optionStatus = document.createElement('option');
            optionStatus.innerHTML = `${data.status}`;
            selectStatus.appendChild(optionStatus);
        }
    })
}


// SIDEBAR

const sidebarHandler = () => {

    let sidebar = document.querySelector('#sidebar');
    let wrapper = document.querySelector('#wrapper');
    let search = document.querySelector('#navbar-search');
    let logo = document.querySelector('#navbar-mobile-logo')
    let navbarList = document.querySelector('#navbar-list')
    let mobileNav = document.querySelector('#navbar-list-mobile');
    let brandNav = document.querySelector('#dekstop-brand');

    if (sidebar.style.left === '0px') {
        sidebar.style.left = '-10%';
        wrapper.style.left = '-8%';
        search.style.display = 'none';
        mobileNav.style.display = 'block';
        navbarList.style.display = 'none';
        logo.style.display = 'block';
        brandNav.style.display = 'none';
    }

    else {
        sidebar.style.left = '0';
        wrapper.style.left = '0';
        search.style.display = 'block';
        mobileNav.style.display = 'none'
        navbarList.style.display = 'block';
        logo.style.display = 'none';
        brandNav.style.display = 'block';
    }
}


//CHART - CHART_COLOR

const chartHandler = (tasks, pending, count) => {

    let existingChart = Chart.getChart('myChart');

    if (existingChart) {
        existingChart.destroy();
    }

    let countOpen = tasks.innerHTML;
    let countPending = pending.innerHTML;
    let countfinishedTasks = count.innerHTML;


    const data = {
        labels: ['Offene Tasks', 'in Bearbeitung', 'Abgeschlossen'],
        datasets: [
            {
                label: 'Task',
                yAxisID: 'Task',
                data: [countOpen, countPending, countfinishedTasks],
                borderColor: getLinearGradient(),
                backgroundColor: 'transparent'
            },
        ]
    };

    const config = {
        type: 'line',
        data: data,
        options: {
            scales: {
            }
        }
    };

    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, config);

}

const getLinearGradient = () => {
    var ctx = document.getElementById('myChart').getContext('2d');
    var gradient = ctx.createLinearGradient(0, 0, 0, 400);
    gradient.addColorStop(0, '#1F75FE');
    gradient.addColorStop(1, '#7DF9FF');
    return gradient;
}