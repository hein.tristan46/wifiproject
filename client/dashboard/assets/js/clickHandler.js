const clickHandler = () => {

    // ALL_PAGES 
    
    reqTasksHandler();
    //loader();
    checkTime();
    document.querySelector('#sidebar-btn').addEventListener('click', sidebarHandler);
    document.querySelector('#startStopBtn').addEventListener('click', startStop);
    document.querySelector('#resetBtn').addEventListener('click', resetTimer)

    // TASK_PAGE

    if(window.location.pathname === '/dashboard/task.html'){
    document.querySelector('#btn-add-task').addEventListener('click', newTask);
    }

    // INDEX_PAGE
    
    if(window.location.pathname === '/dashboard/index.html'){
    getUsername();
    document.querySelector('#addTask').addEventListener('click', modalHandlerIndex)
    document.querySelector('#btn-add-task').addEventListener('click', reqNewTaskIndex);
    document.querySelector('#btn-add-cat').addEventListener('click', addCategoryHandler);
    document.querySelector('#profile-button-second').addEventListener('click', logoutHandler)
    }
}
clickHandler()