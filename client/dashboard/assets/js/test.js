const updateUser = () => {
    let id //JWT TOKEN MITSENDEN UND DARAUS DIE ID AM SERVER LESEN;
    let vn = document.querySelector('#vn').value;
    let nn = document.querySelector('#nn').value;
    let shTxt = document.querySelector('#shTxt').value;
    let txt = document.querySelector('#txt').value;
    let lc = document.querySelector('#lc').value;
    let phone = document.querySelector('#phone').value;
    let psn = document.querySelector('#psn').value;



    fetch('/updateUser', {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `vorname=${vn}&nachname=${nn}&shText=${shTxt}&text=${txt}&lc=${lc}&phone=${phone}&psn=${psn}`
    }).then(resp => resp.text())
    .then(data => {
        console.log(data)
    })
}


document.querySelector('#btn-form').addEventListener('click', updateUser)