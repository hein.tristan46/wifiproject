let timer;
let totalSeconds = 0;

const pad = (num) => {
    return num < 10 ? '0' + num : num;
}

const updateDisplay = () => {
    let hours = Math.floor(totalSeconds / 3600);
    let minutes = Math.floor((totalSeconds % 3600) / 60);
    let seconds = totalSeconds % 60;
    let display = document.getElementById('display');
    display.innerHTML = pad(hours) + ':' + pad(minutes) + ':' + pad(seconds);
} 

const startTimer = () => {
    totalSeconds++;
    updateDisplay();
}

const startStop = () => {
    let timeObject = localStorage.getItem('timer');

    if (timeObject) {
        let data = JSON.parse(timeObject);
        if (data.id === '') {
    
        } else if (!timer) {
            timer = setInterval(startTimer, 1000);
            localStorage.setItem('timer', JSON.stringify({ id: data.id, time: totalSeconds, running: true }));
        } else {
            clearInterval(timer);
            localStorage.setItem('timer', JSON.stringify({ id: data.id, time: totalSeconds, running: false }));
            saveTime(totalSeconds, data.id, data.task);
            timer = null;
        }
    }
}

const resetTimer = (data) => {
    let local = localStorage.getItem('timer')
    newLocal = JSON.parse(local)

    if (newLocal.id === '' || data.id === '') {

    } else {
        clearInterval(timer);
        timer = null;
        totalSeconds = 0;
        localStorage.removeItem('timer');
        saveTime(totalSeconds, newLocal.id);
        updateDisplay();
        location.reload()
    }
}

const setTimerClick = () => {

    let local = localStorage.getItem('timer');
    newLocal = JSON.parse(local);

    clearInterval(timer);
    timer = null
    totalSeconds = newLocal.time;
    updateDisplay();
}

const saveTime = (totalSeconds, id) => {
    fetch(`/saveTimer/${id}`, {
        method: 'PATCH',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: `data=${totalSeconds}`
    }).then(resp => resp.text())
        .then(message => {
            console.log(message)
        })
}

const reqTime = (data) => {
    let id = data.id;

    fetch(`/getTime/${id}`, {
        method: 'get'
    }).then(resp => resp.json())
        .then(resp => {

            let totalSeconds = resp.time.time;
            let hours = Math.floor(totalSeconds / 3600);
            let minutes = Math.floor((totalSeconds % 3600) / 60);
            let seconds = totalSeconds % 60;

            let showTimeModal = document.querySelector('#showTimeModal');
            let bgList = document.querySelector('#list-task-time');
            let showTime = document.querySelector('#display');

            showTime.innerHTML = pad(hours) + ':' + pad(minutes) + ':' + pad(seconds);
          
        })
}

const checkTime = () => {
    let savedTime = localStorage.getItem('timer');
    let newTime = JSON.parse(savedTime);

    if (!newTime) {
        totalSeconds = 0;
    } else {
        totalSeconds = newTime.time;
    }

    updateDisplay(); 
}

window.addEventListener('beforeunload', function(event) {
    let data = JSON.parse(localStorage.getItem('timer'));

    if(data.running == true) {
        localStorage.setItem('timer', JSON.stringify({ id: data.id, time: totalSeconds, running: true }));
    }else if(data.running == false){
        localStorage.setItem('timer', JSON.stringify({ id: data.id, time: totalSeconds, running: false }));
    }
});


window.onload = function() {
    let data = JSON.parse(localStorage.getItem('timer'));

    if(data.running == true) {
        startStop();
    }
    
};