
// JWT - LOADER

const checkToken = async () => {
    let status = document.querySelector('#status')
    let token = sessionStorage.getItem('jwt');


    fetch('/checkToken', {
        method: 'get',
        headers: {
            'Authorization': `Bearer ${token}`,
        },
    }).then(data => {
        if (data.status === 401 || data.status === 403) {
            localStorage.setItem('authenticated', false)
            window.location.href = '../main/login.html';
        }
    })
}

const loader = () => {

    let taskLoader = document.querySelector('#loader');
    taskLoader.style.display = 'block';

    setTimeout(() => {
       reqTasksHandler()
    }, 1000);

}


// TASK

const reqTasksHandler = async () => {
    let taskLoader = document.querySelector('#loader');
    let ul = document.querySelector('#list-task');

    ul.innerHTML = '';
    taskLoader.style.display = 'none';

    fetch('/allTasks', {
        method: 'get'
    }).then(resp => resp.json())
        .then(data => {
            if (data === 204) {
                li = document.createElement('li')
                li.innerHTML = 'KEINE EINTRAG GEFUNDEN';
                ul.appendChild(li);
            } else {
                //PLUGINS.JS
                createMapTask(ul, data);
                categoryHandler();
                statusHandler();
                taskCountHandler();
                checkToken();
            }
        })
}

const newTask = (e) => {
    e.preventDefault();
    let task = document.querySelector('#newTask').value;
    let cat = document.querySelector('#select-category').value;
    let statusTask = document.querySelector('#select-status').value;
    let status = document.querySelector('#status-task');

    if (task === '' || cat === '') {
        status.innerHTML = 'Bitte geben Sie einen Wert ein';
        status.style.color = 'red';
    } else {
        status.innerHTML = '';
        fetch('/addTask', {
            method: 'post',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: `task=${task}&category=${cat}&status=${statusTask}`
        }).then(response => response.json())
            .then(data => {
                if (data) {
                    status.style.display = 'block';
                    status.innerHTML = 'Task wurde erfolgreich hinzugefügt';
                    status.style.color = 'green';
                    status.classList.add('success');

                    let timeObject = {
                        id: data.id,
                        time: data.time
                    }
                    let saveObject = JSON.stringify(timeObject)
                    localStorage.setItem('timer', saveObject);

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else if(!data || data == "Task already in use") {
                    status.style.display = 'block';
                    status.innerHTML = 'Fehler beim hinzufügen vom neuen Task';
                    status.style.color = 'red';
                }
            })
    }
}

const updateTask = (data) => {
    let id = data.id;

    let updateTaskInput = document.querySelector('#updateTask');
    let updateTask = updateTaskInput.value;
    let updateCat = document.querySelector('#info-select-category').value;
    let updateStatus = document.querySelector('#info-select-status').value;
    let status = document.querySelector('#status-task');

    /// FEHLER MIT SINNLOSEN ZEICHEN
    if (updateTask === '') {
        updateTask = updateTaskInput.placeholder;
    }
    fetch(`/updateTask/${id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `task=${updateTask}&cat=${updateCat}&status=${updateStatus}`
    }).then(resp => resp.text())
        .then(data => {
            if (data) {
                status.style.display = 'block';
                status.innerHTML = 'Task wurde erfolgreich aktualisiert';
                status.style.color = 'green';
                status.classList.add('success');

                setTimeout(function () {
                    location.reload();
                }, 1000);
            } else {
                status.style.display = 'block';
                status.innerHTML = 'Fehler beim hinzufügen vom neuen Task';
                status.style.color = 'red';
            }
        })
}

const deleteTask = async (data) => {
    let id = data.id;

    let emptyObject = {
        id: false,
        time: false
    }
    let saveObject = JSON.stringify(emptyObject)
    localStorage.setItem('timer', saveObject);

    fetch(`/deleteTask/${id}`, {
        method: 'DELETE',
    }).then(resp => resp.json())
        .then(data => {
            reloadPage()
        })
}

const reloadPage = () => {
    location.reload();
}


// CATEGORY

const categoryHandler = async () => {
    fetch('/allCategory', {
        method: 'get',

    }).then(resp => resp.json())
        .then(data => {
            createMapCategory(data)
        })
}

const addCategoryHandler = (e) => {
    e.preventDefault();

    let select = document.querySelector('#newCategory').value;
    let status = document.querySelector('#status-task');

    if (select === '') {
        status.innerHTML = 'Bitte geben Sie einen Wert ein';
        status.style.color = 'red';
    } else {
        status.innerHTML = '';
        fetch('/addCategory', {
            method: 'post',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: `cat=${select}`
        }).then(data => {
            if (data) {
                status.style.display = 'block';
                status.innerHTML = 'Kategorie wurde erfolgreich hinzugefügt';
                status.style.color = 'green';
                status.classList.add('success');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            } else {
                status.style.display = 'block';
                status.innerHTML = 'Fehler beim hinzufügen von neuer Kategorie';
                status.style.color = 'red';
            }
        })
    }
}

const modalHandler = () => {
    let modal = document.querySelector('#myModal');

    if (modal.style.display === 'none' || modal.style.display === '') {
        modal.style.display = 'block';
    }
    document.querySelector('#close-btn').addEventListener('click', () => {
        modal.style.display = 'none';
    });
}

const commentModalHandler = (data) => {
    let commentModal = document.querySelector('#commentModal');

    if (commentModal.style.display === 'none' || commentModal.style.display === '') {
        commentModal.style.display = 'block';

    }
    document.querySelector('#comment-close-btn').addEventListener('click', () => {
        commentModal.style.display = 'none';
    });
}


// COMMENT

const commentHandler = async (data) => {

    let comment = document.querySelector('#comment').value.trim();
    let id = data.commentId;

    if (id == undefined && comment) {
        console.log('Noch kein commentId aber input beschrieben');
        //newComment(comment, data);
    }
    if (comment && id) {
        console.log('CommentId vorhanden und input beschrieben');
        //updateComment(comment, data);
        //newComment(comment, data);
    }

    if (id == undefined && !comment) {
        console.log('Keine Daten');
    }

    if (data.commentId && !comment) {
        //updateTask(id);
    }

}

const newComment = async (comment, data) => {
    let status = document.querySelector('#status-task');
    let id = data.id;

    fetch(`/commentTask/${id}`, {
        method: "post",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `comment=${comment}`
    }).then(resp => resp.text())
        .then(data => {
            getComments();
        })
}

const getComments = async (data) => {

    let id = data.commentId;

    fetch(`/getComment/${id}`, {
        method: "get"
    }).then(resp => resp.json())
        .then(data => {
            JSON.stringify(data)


            let insertComment = document.querySelector('#commentList');
            insertComment.innerHTML = '';

            if (Object.getOwnPropertyNames(data).length === 0) {
                let insertComment = document.querySelector('#commentList');
                insertComment.placeholder = 'Kein Kommentar ...';
            }
            else {

                data.foundComments.forEach(commentData => {
                    let comments = document.createElement('li');
                    comments.innerHTML = commentData.comment;
                    comments.classList.add('list-group-item');
                    insertComment.appendChild(comments);
                })
            }
        })
}

const updateComment = async (comment, data) => {

    let id = data.commentId;
    fetch(`/updateComment/${id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `comment=${comment}`
    }).then(resp => resp.text())
        .then(data => {
            location.reload();
        })
}


// STOPWATCH

const timeHandler = async (data) => {
    let saveBtn = document.querySelector('#saveBtn');

    saveBtn.addEventListener('click', () => {
        let timeDates = JSON.stringify(data);
        let id = localStorage.getItem('timer');
        fetch(`/saveTimer/${id}`, {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            // body: `stn=${stn}&min=${min}&sek=${sek}`
            body: `data=${timeDates}`
        }).then(resp => resp.text())
            .then(message => {
                console.log(message)
            })
    })

}


// STATUS 

const statusHandler = () => {
    let status = document.querySelector('#select-status');

    fetch('/allStatus', {
        method: 'get'
    }).then(resp => resp.json())
        .then(data => {
            if (data) {
                createMapStatus(data)
            }
        })
}


// INDEXPAGE 

const reqNewTaskIndex = (e) => {
    e.preventDefault();
    let task = document.querySelector('#newTask').value;
    let cat = document.querySelector('#select-category').value;
    let status = document.querySelector('#status-task');

    if (task === '' || cat === '') {
        status.innerHTML = 'Bitte geben Sie einen Wert ein';
        status.style.color = 'red';
    } else {
        status.innerHTML = '';
        fetch('/addTask', {
            method: 'post',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: `task=${task}&category=${cat}`
        }).then(response => response.json())
            .then(data => {

                if (data) {
                    status.style.display = 'block';
                    status.innerHTML = 'Task wurde erfolgreich hinzugefügt';
                    status.style.color = 'green';
                    status.classList.add('success');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else {
                    status.style.display = 'block';
                    status.innerHTML = 'Fehler beim hinzufügen vom neuen Task';
                    status.style.color = 'red';
                }
            })
    }
}

const modalHandlerIndex = () => {
    let modal = document.querySelector('#modal');

    if (modal.style.left === '0px' || modal.style.display === '') {
        modal.style.display = 'block';
    }
    document.querySelector('#close-btn').addEventListener('click', () => {
        modal.style.display = 'none';
    });
}

const getUsername = () => {

    let user = sessionStorage.getItem('user');
    let insertUsername = document.querySelector('#profile-username');
    let cardUsername = document.querySelector('#card-username');

    cardUsername.innerHTML = user;
    insertUsername.innerHTML = user;
}

const logoutHandler = () => {

    let jwt = sessionStorage.getItem('jwt');

    fetch('/logout', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `jwt=${jwt}`
    }).then(resp => resp.text())
    .then(data => {
        sessionStorage.removeItem('jwt');
        window.location.href = '../main/login.html'
    })
}